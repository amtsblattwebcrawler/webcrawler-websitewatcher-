import hashlib
import boto3
from bs4 import BeautifulSoup as bs
import requests
from datetime import datetime

session = boto3.Session(region_name='eu-central-1',
                        aws_access_key_id='AKIAJHK4UKVNUFHNWRVQ',
                        aws_secret_access_key='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')

class websiteHasher:
    def __init__(self,url_id,url_to_hash,name):
        self.url_id = url_id
        self.url_to_hash = url_to_hash
        self.city_name= name

    def generateHash(self):
        try:
            cleanedHTML = HTMLSelector(requests.get(self.url_to_hash).text).selectText()
            return hashlib.sha224(cleanedHTML).hexdigest()
        except requests.exceptions.ConnectionError as e:
           # lgr.info("FAILED: Connect to {0} time out. Unable to hash Website ".format(self.url_to_hash))
            return None

    def getLastHash(self):
        conn = session.resource('dynamodb')
        table = conn.Table('seeds')
        print(self.url_id)
        res = table.get_item(
            Key={
                'ID': str(self.url_id)
            }
        )
        print(res)
        return res['Item']['lastHashValue']

    def updateHash(self,new_hash_value):
        conn = session.resource('dynamodb')
        table = conn.Table('seeds')
        table.update_item(
            Key={
                'ID': str(self.url_id)
            },
            UpdateExpression="SET lastHashValue = :var1, lastModified= :var2",
            ExpressionAttributeValues={
                ':var1': new_hash_value,
                ':var2': str(datetime.now().date())
            }
        )


    def compareHash(self):
        lastHashValue = self.getLastHash()
        currentHashValue = self.generateHash()
        if lastHashValue != None:
            if currentHashValue != lastHashValue:
#            #     global changes
            #     changes += 1
                self.updateHash(currentHashValue)
                self.send_message_to_queue()

               # send_message_to_queue(self.url_id, self.url_to_hash)
            #else:
#              lgr.info(" ".format(self.url_to_hash))
       # else:
#            lgr.info("FAILED: {0} doesn't have any new hash to compare ".format(self.url_to_hash))

    def send_message_to_queue (self):
        session.client('sqs').send_message(
            QueueUrl = 'https://sqs.eu-central-1.amazonaws.com/419206837402/cityURLqueue_v1',
            MessageBody=str(self.url_to_hash),
            MessageAttributes={
                'parent_URL_ID':{
                    'DataType':'String',
                    'StringValue': str(self.url_id)
                },
                'Name': {
                    'DataType': 'String',
                    'StringValue': str(self.city_name)
                },
            }
        )


class HTMLSelector:
    def __init__(self,html):
        self.html = html

    ''''Select only the javascript and styles in the html document'''
    def selectJavascript(self):
        soup = bs(self.html,features='html.parser')
        return [x.extract() for x in soup.findAll(['script','style'])]

    def selectText(self):
        soup = bs(self.html,features='html.parser')
        for x in soup.findAll(['script','style']):
            x.decompose()
        text = soup.get_text()
        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)
        # encode text
        return text.encode('utf-8')




def lambda_handler(event, context):
    for record in event['Records']:
        url = str(record['body'])
        payload= record['messageAttributes']
        id= payload['ID']['stringValue']
        name= payload['Name']['stringValue']
        hasher = websiteHasher(id,url,name)
        hasher.compareHash()

if __name__== "__main__":

    event={
       "Records":[
          {
             "messageId":"d084600a-18ff-44cd-b1e2-fabc29c6a0d9",
             "receiptHandle":"AQEBHAjlZo7sBXlWVZ8+AwY+EUmB/oHvAKYLyj1MVi0Th8/fSpyv3E+txJNwx+/XvAbDkdv4s379WLcn+sBMYCXd+IJcD/RNN5tKxvY9g2mqvWdtI0ivIjbZ3h68nmXrtxHInc+h1zN36vCJTheFBcWgVtT914YWHxcY8OmZrabRGAq2RwHaWuCyIe88MNLMGya1BCY0c0Pt/A/cMdH4f65iU2UF9FQvI/BYUkQdiQa5jpJ32M/hcFjQBk7NolMYKyUzLwzzlIVBCsqzKpnHAXXkiL4rDmA86G+XdkbzZFM9nLUhr1/8M1xzRJGabKNznpnDsFg/z7UX3J0boqiNsIfkh95qxGK68SUhc3DJKR5xb4BeOOsDffFeB4Ft2pRJNkzf9LLnmDyLexgJivmgD8gjIA==",
             "body":"https://www.kreis-warendorf.de/aktuelles/amtsblatt/?no_cache=1",
             "attributes":{
                "ApproximateReceiveCount":"1518",
                "SentTimestamp":"1560686919713",
                "SenderId":"AIDAJP5ABP3NDYUP5ZG6U",
                "ApproximateFirstReceiveTimestamp":"1560686919713"
             },
             "messageAttributes":{
                "ID":{
                   "stringValue":"42c996b0-f453-5f94-ab8a-42d13570e6d1",
                   "stringListValues":[
                   ],
                   "binaryListValues":[

                   ],
                   "dataType":"String"
                }
             },
             "md5OfBody":"4fedd9875d9dce840144c80bbdd15473",
             "md5OfMessageAttributes":"88709543ce1ac023d1f4bd49bf0f06cb",
             "eventSource":"aws:sqs",
             "eventSourceARN":"arn:aws:sqs:eu-central-1:419206837402:seeds",
             "awsRegion":"eu-central-1"
          }
       ]
    }
    context=1
    lambda_handler(event,context)

